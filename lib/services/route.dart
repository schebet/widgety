import 'package:flutter/material.dart';
import 'package:widgety_app/screens/grid.dart';
import 'package:widgety_app/screens/screen_wrapper.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    case '/':
      return MaterialPageRoute(builder: (context) => ScreenWrapper());
    case '/grid':
      var appbar = settings.arguments;
      return MaterialPageRoute(builder: (context) => Grid());
  }
}
