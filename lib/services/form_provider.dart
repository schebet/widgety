import 'package:flutter/material.dart';

class FormProvider with ChangeNotifier{
  String _name='',_item='', _appbar='';

  String get name => _name;
  String get item => _item;
  String get appbar => _appbar;

   setData(name, item, appbar){
   _name = name;
   _item = item;
   _appbar =appbar;
     notifyListeners();
   }
  

}