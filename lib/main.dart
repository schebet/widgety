import 'package:flutter/material.dart';
import 'package:widgety_app/services/form_provider.dart';
import 'services/route.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(providers: [
          ChangeNotifierProvider<FormProvider>(create: (_) => FormProvider()),

    ], child:
     Theme(
      data: ThemeData(
          primaryColor: Colors.blueGrey),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        initialRoute: "/",
        onGenerateRoute: generateRoute,
      ),
    ) );
    
    
  }
}
