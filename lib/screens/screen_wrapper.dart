import 'package:flutter/material.dart';
import 'package:widgety_app/screens/grid.dart';
import 'home.dart';

class ScreenWrapper extends StatefulWidget {
  @override
  _ScreenWrapperState createState() => _ScreenWrapperState();
}

class _ScreenWrapperState extends State<ScreenWrapper> {
  int _currentIndex = 0;

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }

  final List<Widget> _children = [Home(),Grid()];
  @override
  Widget build(BuildContext context) {
    return Theme(
                    data: ThemeData(
                primaryColor: Colors.blueGrey,
              ),
          child: Scaffold(
        body: _children[_currentIndex],
        bottomNavigationBar: BottomNavigationBar(
          onTap: onTabTapped,
          currentIndex: _currentIndex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home,),
              title: Text('Home'),
            ),

            BottomNavigationBarItem(
              icon: Icon(Icons.settings),
              title: Text('Details'),
            )
          ],
        ),
      ),
    );
  }
}
