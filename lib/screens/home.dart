import 'package:flutter/material.dart';
import 'package:widgety_app/services/form_provider.dart';
import 'package:provider/provider.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  var item;
  var name;
  var appbar = 'WIDGETY';
  @override
  Widget build(BuildContext context) {
    var appBarTitleText = Text(
                'WIDGETY',
                style: TextStyle(fontFamily: 'Tenor Sans'),
              );
    
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
            backgroundColor: Colors.blueGrey,
            leading: Icon(Icons.timeline_rounded),
            foregroundColor: Colors.orange,
            elevation: 2,
            expandedHeight: 150.0,
            flexibleSpace: FlexibleSpaceBar(
              title: appBarTitleText,
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.menu),
                tooltip: 'Add new entry',
                onPressed: () {/* ... */},
              ),
            ]),
        SliverToBoxAdapter(
            child: Column(
          children: [
            Card(
              child: Form(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Column(
                        children: [
                          Column(
                            children: [
                              TextFormField(
                                  decoration: const InputDecoration(
                                    icon: Icon(Icons.person),
                                    hintText: 'Type your name',
                                    labelText: 'Name',
                                  ),
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'Please enter some text';
                                    }
                                    return null;
                                  },
                                  onChanged: (val) {
                                    setState(() {
                                      name = val;
                                    });
                                  }),
                              DropdownButtonFormField<String>(
                                decoration: const InputDecoration(
                                  icon: Icon(Icons.sync_alt),
                                  labelText: 'Choose Stack',
                                ),
                                value: item,
                                items: [
                                  'Flutter',
                                  'Adonis Js',
                                  'Vue Js',
                                  'Quarkus',
                                  'Go'
                                ]
                                    .map((label) => DropdownMenuItem(
                                          child: Text(label),
                                          value: label,
                                        ))
                                    .toList(),
                                onChanged: (value) {
                                  setState(() {
                                    item = value;
                                  });
                                },
                              ),
                            ],
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 250,
                      ),

                      RaisedButton(
                        onPressed: () {
                          context.read<FormProvider>().setData(name, item, appbar);
                          Navigator.pushNamed(context, '/grid', arguments: {
                                 'appbar': 'WIDGETY',

                          });
                        },
                        child: Text('View'),
                        color: Colors.deepOrange,
                        textColor: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20.0),
                            side: BorderSide(color: Colors.red)),
                      ),

                      //SizedBox(height: 60,),
                    ],
                  ),
                ),
              ),
            )
          ],
        )),
      ]),
    );
  }
}
