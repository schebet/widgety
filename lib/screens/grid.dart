import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:widgety_app/services/form_provider.dart';

class Grid extends StatefulWidget {
  
  @override
  _GridState createState() => _GridState();
}

class _GridState extends State<Grid> {

  @override
  
  Widget build(BuildContext context) {
    var state = context.watch<FormProvider>();

    var appbar = Text(
                '${state.appbar}',
                style: TextStyle(fontFamily: 'Tenor Sans'),
              );
    return Scaffold(
      body: CustomScrollView(slivers: [
        SliverAppBar(
            backgroundColor: Colors.blueGrey,
            pinned: true,
            leading: Icon(Icons.timeline_rounded),
            foregroundColor: Colors.orange,
            elevation: 2,
            expandedHeight: 150.0,
            flexibleSpace: FlexibleSpaceBar(
              title: appbar,
            ),
            actions: <Widget>[
              IconButton(
                icon: const Icon(Icons.person),
                tooltip: 'Add new entry',
                onPressed: () {/* ... */},
              ),
            ]),
        SliverToBoxAdapter(
          child: Card(
            child:Column(children: [
              Text('${state.name}'),
              Text('${state.item}')
            ],)
          ),
        ),
        SliverPadding(
          padding: EdgeInsets.all(8.0),
          sliver: SliverGrid.count(
            crossAxisCount: 2,
            children: [
              Container(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://flutter.dev/images/catalog-widget-placeholder.png"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text('Flutter',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text(' Mobile'),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ),
              Container(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://design.jboss.org/quarkus/logo/images/quarkus_blogpost_icon.png"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text('Quarkus',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text('Backend'),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ),
              Container(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://download.logo.wine/logo/Go_(programming_language)/Go_(programming_language)-Logo.wine.png"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text('Go',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text('Backend'),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ),
              Container(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://miro.medium.com/max/2400/1*-8AAdexfOAK9R-AIha_PBQ.png"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text('Vue Js',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text(
                            'Frontend - Web',
                          ),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ),
              Container(
                child: GestureDetector(
                  behavior: HitTestBehavior.opaque,
                  child: Card(
                    elevation: 2,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.width / 4,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: NetworkImage(
                                  "https://www.pngrepo.com/png/305665/180/adonis-js.png"),
                              fit: BoxFit.fitWidth,
                              alignment: Alignment.topCenter,
                            ),
                          ),
                        ),
                        ListTile(
                          title: Text('Adonis Js',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold)),
                          subtitle: Text(
                            'Backend',
                          ),
                        ),
                      ],
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20)),
                  ),
                ),
              ),
            ],
          ),
        ),
      ]),
    );
  }
}
