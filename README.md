# widgety_app

A simple flutter app with a splash screen, some form elements and slivers.

## SCREENSHOTS
<p float="left">
  <img src="https://user-images.githubusercontent.com/57434209/116680192-10714c00-a9b4-11eb-8477-edb46af1f03a.png" width="250" />
    <img src="https://user-images.githubusercontent.com/57434209/116680139-ff283f80-a9b3-11eb-830b-39a899749b4b.png" width="250" />
  <img src="https://user-images.githubusercontent.com/57434209/116680441-5cbc8c00-a9b4-11eb-8397-1f612282854f.png" width="250" />
</p>
<p float="left">
  <img src="https://user-images.githubusercontent.com/57434209/116680364-3f87bd80-a9b4-11eb-8360-b71040017593.png" width="250" />
  <img src="https://user-images.githubusercontent.com/57434209/116680408-51696080-a9b4-11eb-8e52-5299a07aa92d.png" width="250" />
</p>

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
